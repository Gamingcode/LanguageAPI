package de.gamingcode.languageapi.messages;

import javax.annotation.Nullable;
import org.bukkit.entity.Player;

/**
 * Created by Gamingcode on 13.02.2021
 */

public interface MessageRepository {
  String lookup(String paramString1, String paramString2, String... paramVarArgs);

  String lookup(Player paramPlayer, String paramString, String... paramVarArgs);

  void sendMessage(Player paramPlayer, String paramString, String... paramVarArgs);

  @Nullable
  MessageEntry lookupKey(String paramString1, String paramString2);
}

