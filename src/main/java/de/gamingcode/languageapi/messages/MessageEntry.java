package de.gamingcode.languageapi.messages;

import com.google.common.base.Preconditions;
import java.util.Objects;

/**
 * Created by Gamingcode on 13.02.2021
 */

public final class MessageEntry {
  private final String locale, content;

  private MessageEntry(String locale, String content) {
    this.locale = locale;
    this.content = content;
  }

  public static MessageEntry of(String locale, String content) {
    Preconditions.checkNotNull(locale);
    Preconditions.checkNotNull(content);
    return new MessageEntry(locale, content);
  }

  public String locale() {
    return this.locale;
  }

  public String content() {
    return this.content;
  }

  public boolean equals(Object other) {
    if (other == this)
      return true;
    if (!(other instanceof MessageEntry))
      return false;
    MessageEntry messageEntry = (MessageEntry)other;
    return (messageEntry.locale().equals(this.locale) && messageEntry.content().equals(this.content));
  }

  public int hashCode() {
    return Objects.hash(this.locale, this.content);
  }

  public String toString() {
    return "MessageEntry{locale='" + this.locale + '\'' + ", content='" + this.content + '\'' + '}';
  }
}
