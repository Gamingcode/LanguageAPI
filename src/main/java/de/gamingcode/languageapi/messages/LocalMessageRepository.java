package de.gamingcode.languageapi.messages;

import com.google.common.base.Preconditions;
import javax.annotation.Nullable;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * Created by Gamingcode on 13.02.2021
 */

public final class LocalMessageRepository implements MessageRepository {
  private final MessageRegistry messages;

  private final Plugin plugin;

  private LocalMessageRepository(MessageRegistry registry, Plugin plugin) {
    this.messages = registry;
    this.plugin = plugin;
  }

  public static LocalMessageRepository create(MessageRegistry registry, Plugin plugin) {
    Preconditions.checkNotNull(registry);
    Preconditions.checkNotNull(plugin);
    return new LocalMessageRepository(registry, plugin);
  }

  @Nullable
  public String lookup(String locale, String key, String... replacements) {
    MessageEntry messageEntry = lookupKey(locale, key);
    if (messageEntry == null) {
      this.plugin
          .getLogger()
          .warning("Didn't find an entry for " + key + " in language " + locale + "!");
      return null;
    }
    String content = messageEntry.content();
    return replacePlaceholders(content, replacements);
  }

  public String lookup(Player player, String key, String... replacements) {
    String locale = player.spigot().getLocale().toLowerCase();
    return lookup(locale, key, replacements);
  }

  private String replacePlaceholders(String content, String... replacements) {
    for (String replacement : replacements) {
      String[] keyValues = replacement.split("->");
      String replacementKey = keyValues[0];
      String replacementValue = keyValues[1];
      if (content.contains(replacementKey))
        content = content.replace(replacementKey, replacementValue);
    }
    return content;
  }

  public void sendMessage(Player player, String key, String... replacements) {
    player.sendMessage(lookup(player, key, replacements));
  }

  public MessageEntry lookupKey(String locale, String key) {
    locale = locale.split("_")[0];
    if (!this.messages.cachedLanguage(locale))
      locale = "en";
    return this.messages.entriesForLocale(locale).getOrDefault(key, null);
  }
}

