package de.gamingcode.languageapi.util;

import com.google.common.base.Preconditions;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 * Created by Gamingcode on 13.02.2021
 */

public final class ConfigurationReader {
  private final String directory;

  private ConfigurationReader(String directory) {
    this.directory = directory;
  }

  public static ConfigurationReader of(String directory) {
    Preconditions.checkNotNull(directory);
    return new ConfigurationReader(directory);
  }

  public YamlConfiguration loadConfiguration(String fileName) {
    YamlConfiguration configuration = new YamlConfiguration();
    fileName = this.directory + "//" + fileName;
    Path path = Paths.get(fileName);
    try {
      if (Files.notExists(path))
        Files.createFile(path);
      configuration.load(Files.newBufferedReader(path, StandardCharsets.UTF_8));
    } catch (IOException|org.bukkit.configuration.InvalidConfigurationException e) {
      e.printStackTrace();
    }
    setDefaults(configuration);
    return configuration;
  }

  private void setDefaults(Configuration configuration) {
    if (configuration.getDefaults() != null)
      configuration
          .getValues(true)
          .forEach((key, value) -> configuration.getDefaults().set(key, null));
  }
}