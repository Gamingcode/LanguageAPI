package de.gamingcode.languageapi.messages;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import de.gamingcode.languageapi.util.ConfigurationReader;
import java.util.Map;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

/**
 * Created by Gamingcode on 13.02.2021
 */

public final class MessageRegistry {
  private final Map<String, Map<String, MessageEntry>> messages = Maps.newHashMap();

  private final Plugin plugin;

  private final ConfigurationReader reader;

  private MessageRegistry(Plugin plugin, ConfigurationReader reader) {
    this.plugin = plugin;
    this.reader = reader;
    registerMessages();
  }

  public static MessageRegistry create(Plugin plugin, ConfigurationReader reader) {
    Preconditions.checkNotNull(plugin);
    Preconditions.checkNotNull(reader);
    return new MessageRegistry(plugin, reader);
  }

  public void registerMessages() {
    long start = System.currentTimeMillis();
    for (String locale : this.plugin.getConfig().getStringList("locales"))
      registerMessagesForLocale(locale);
    long duration = System.currentTimeMillis() - start;
    this.plugin
        .getLogger()
        .info("Registered messages for " + this.messages.size() + " language(s). (" + duration + "ms)");
  }

  private void registerMessagesForLocale(String locale) {
    YamlConfiguration configuration = this.reader.loadConfiguration("language_" + locale + ".yml");
    String prefix = configuration.getString("prefix");
    for (String key : configuration.getKeys(true))
      registerMessage(configuration, key, prefix, locale);
  }

  private void registerMessage(YamlConfiguration configuration, String key, String prefix, String locale) {
    if (configuration.isString(key)) {
      String raw = configuration.getString(key);
      String content = ChatColor.translateAlternateColorCodes('&', raw.replace("%prefix%", prefix));
      appendOrCreateMessageEntry(key, content, locale);
    }
  }

  private void appendOrCreateMessageEntry(String key, String content, String locale) {
    MessageEntry messageEntry = MessageEntry.of(locale, content);
    Map<String, MessageEntry> entries = this.messages.getOrDefault(locale, Maps.newHashMap());
    entries.put(key, messageEntry);
    this.messages.put(locale, entries);
  }

  public boolean cachedLanguage(String locale) {
    return this.messages.containsKey(locale);
  }

  public Map<String, MessageEntry> entriesForLocale(String locale) {
    Preconditions.checkNotNull(locale);
    return this.messages.get(locale);
  }
}

