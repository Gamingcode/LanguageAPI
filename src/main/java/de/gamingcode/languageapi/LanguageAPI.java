package de.gamingcode.languageapi;

import de.gamingcode.languageapi.messages.LocalMessageRepository;
import de.gamingcode.languageapi.messages.MessageRegistry;
import de.gamingcode.languageapi.messages.MessageRepository;
import de.gamingcode.languageapi.util.ConfigurationReader;
import org.bukkit.plugin.Plugin;

/**
 * Created by Gamingcode on 13.02.2021
 */

public final class LanguageAPI {

  private final MessageRepository messages;

  public MessageRepository getMessages() {
    return this.messages;
  }

  public LanguageAPI(Plugin plugin) {
    MessageRegistry registry = MessageRegistry.create(plugin,
        ConfigurationReader.of(plugin.getDataFolder().getAbsolutePath()));
    this.messages = LocalMessageRepository.create(registry, plugin);
  }
}